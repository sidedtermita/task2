package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.task2.dao.ListaDao;
import com.example.task2.dao.impl.ListaDaoImpl;
import com.example.task2.entity.Lista;
import com.example.task2.entity.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.List;

public class ShowList extends AppCompatActivity {
    private RecyclerView recyclerView;
    private MyAdapter mAdapter;
    private static final String TAG = "ShowListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_list);
        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        User user = (User) intent.getSerializableExtra(MainActivity.EXTRA_USER);
        Log.i(TAG,"user:: " + user);
        ImageView avatar = findViewById(R.id.imageView);
        assert user != null;
        int drawableResourceId = this.getResources().getIdentifier(user.getAvatar(), "drawable", this.getPackageName());
        avatar.setImageResource(drawableResourceId);

        TextView txtViewTitle = (TextView) findViewById(R.id.usernameValue);
        txtViewTitle.setText(user.getUsername());

        TextView txtViewName = (TextView) findViewById(R.id.nameValue);
        txtViewName.setText(user.getFirstName());

        String lastAccess = intent.getStringExtra("ULTIMO_ACCESO");
        TextView txtViewLastAccess = (TextView) findViewById(R.id.lastAccessValue);
        txtViewLastAccess.setText(lastAccess);

        //myToolbar.setLogo(R.drawable.);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        final ListaDao listaDao = new ListaDaoImpl();
        final List<Lista> myDataSet = listaDao.getListsByUser(user.getUsername(),getApplicationContext());
        mAdapter = new MyAdapter(myDataSet,getApplicationContext());
        recyclerView.setAdapter(mAdapter);//recyclerView

        recyclerView.addOnItemTouchListener(new RecyclerItemListener(
                getApplicationContext(),
                recyclerView,
                new RecyclerItemListener.RecyclerTouchListener() {
                    @Override
                    public void onClickItem(View v, int position) {
                        Log.i(TAG,"On Click Item interface " + v + "," + position);
                        TextView txt = v.findViewById(R.id.itemValueId);
                        String idStr = txt.getText().toString();
                        Log.i(TAG,"idStr=" + idStr);
                        Lista lista = listaDao.getListaById(idStr,getApplicationContext());
                        Log.i(TAG,"invocando dialogo...");
                        DialogFragment newFragment = new DeleteTitleDialog(lista,recyclerView);
                        newFragment.show(getSupportFragmentManager(), "deleteTitle");
                    }

                    @Override
                    public void onLongClickItem(View v, int position) {
                        Log.i(TAG,"On Click Item interface " + v + "," + position);
                    }
                }));

        FloatingActionButton fab = findViewById(R.id.floatingActionButton3);
        final String username = user.getUsername();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i(TAG,"invocando dialogo...");
                DialogFragment newFragment = new NewTitleDialog(username,listaDao,recyclerView);
                newFragment.show(getSupportFragmentManager(), "newTitle");
            }
        });

        /*
        OnItemActivatedListener<Long> myItemActivatedListener = new OnItemActivatedListener<Long>() {
            @Override
            public boolean onItemActivated(@NonNull ItemDetailsLookup.ItemDetails<Long> item, @NonNull MotionEvent e) {
                return true;
            }
        };

        SelectionTracker tracker = new SelectionTracker.Builder<>(
                "my-selection-id",
                recyclerView,
                new StableIdKeyProvider(recyclerView),
                new MyDetailsLookup(recyclerView),
                StorageStrategy.createLongStorage())
                .withOnItemActivatedListener(myItemActivatedListener)
                .build();

         */
    }

    @Override
    protected void onDestroy() {
        Log.i(TAG,"destruyendo ...");
        super.onDestroy();
    }
}