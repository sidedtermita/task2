package com.example.task2.entity;

import java.io.Serializable;

public class Lista implements Serializable
{
    private Integer id;
    private String title;
    private String idUsername;
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getIdUsername() {
        return idUsername;
    }
    public void setIdUsername(String idUsername) {
        this.idUsername = idUsername;
    }
    @Override
    public String toString() {
        return "Lista{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", idUsername='" + idUsername + '\'' +
                '}';
    }
}
