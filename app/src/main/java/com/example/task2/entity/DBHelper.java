package com.example.task2.entity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper
{
    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "super.db";

    private static final String SQL_CREATE_USERS =
            "CREATE TABLE " + DBContract.User.TABLE_NAME + " (" +
                    DBContract.User._ID + " INTEGER PRIMARY KEY," +
                    DBContract.User.COLUMN_NAME_USERNAME + " TEXT," +
                    DBContract.User.COLUMN_NAME_PASSWORD + " TEXT," +
                    DBContract.User.COLUMN_NAME_FIRST_NAME + " TEXT," +
                    DBContract.User.COLUMN_NAME_LAST_NAME + " TEXT," +
                    DBContract.User.COLUMN_NAME_LAST_ACCESS + " TEXT," +
                    DBContract.User.COLUMN_NAME_AVATAR + " TEXT" +
                    ")";
    private static final String SQL_CREATE_LISTAS =
            "CREATE TABLE " + DBContract.Lista.TABLE_NAME + " (" +
                    DBContract.Lista._ID + " INTEGER PRIMARY KEY," +
                    DBContract.Lista.COLUMN_NAME_TITLE + " TEXT," +
                    DBContract.Lista.COLUMN_NAME_USERNAME + " TEXT" +
                    ")";

    private static final String SQL_DELETE_USERS =
            "DROP TABLE IF EXISTS " + DBContract.User.TABLE_NAME;
    private static final String SQL_DELETE_LISTAS =
            "DROP TABLE IF EXISTS " + DBContract.Lista.TABLE_NAME;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_USERS);
        sqLiteDatabase.execSQL(SQL_CREATE_LISTAS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        sqLiteDatabase.execSQL(SQL_DELETE_USERS);
        sqLiteDatabase.execSQL(SQL_DELETE_LISTAS);
        onCreate(sqLiteDatabase);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
