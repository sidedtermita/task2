package com.example.task2.entity;

import android.provider.BaseColumns;

public final class DBContract {

    private DBContract(){}

    public static class User implements BaseColumns
    {
        public static final String TABLE_NAME = "users";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_FIRST_NAME = "first_name";
        public static final String COLUMN_NAME_LAST_NAME = "last_name";
        public static final String COLUMN_NAME_LAST_ACCESS = "last_access";
        public static final String COLUMN_NAME_AVATAR = "avatar";
    }

    public static class Lista implements BaseColumns
    {
        public static final String TABLE_NAME = "lista";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_USERNAME = "username";
    }


}
