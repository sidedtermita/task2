package com.example.task2;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.example.task2.dao.ListaDao;
import com.example.task2.entity.Lista;
import java.util.List;

public class NewTitleDialog extends DialogFragment {
    private static final String TAG = "NewTitleDialog";
    private String username;
    private ListaDao listaDao;
    private RecyclerView recyclerView;

    public NewTitleDialog(String username, ListaDao listaDao,RecyclerView myRecyclerView)
    {
        this.username = username;
        this.listaDao = listaDao;
        this.recyclerView = myRecyclerView;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        final LayoutInflater inflater = requireActivity().getLayoutInflater();
        builder.setTitle("Nuevo titulo");
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.my_custom_view_dialog, null))
                // Add action buttons
                .setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText editText = getDialog().findViewById(R.id.newTitle);
                        String newTitle = editText.getText().toString();
                        Log.i(TAG,"conectando a db...");
                        Lista lista = new Lista();
                        lista.setTitle(newTitle);
                        lista.setIdUsername(username);
                        listaDao.insert(lista,getContext());
                        List<Lista> myDataSet = listaDao.getListsByUser(username,getContext());
                        MyAdapter myAdapter = new MyAdapter(myDataSet, getContext());
                        myAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(myAdapter);//recyclerView
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        dialog.cancel();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG,"destruyendo dialogo...");
        super.onDestroy();
    }
}
