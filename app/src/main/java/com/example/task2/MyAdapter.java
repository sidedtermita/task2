package com.example.task2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.selection.ItemDetailsLookup;
import androidx.recyclerview.widget.RecyclerView;
import com.example.task2.entity.Lista;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{
    private List<Lista> mDataset;
    private Context context;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(List<Lista> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }


    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.my_text_view, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.textView.getLayout().
        holder.txtViewId.setText(mDataset.get(position).getId().toString());
        holder.txtViewTitle.setText(mDataset.get(position).getTitle());
        holder.txtViewUsername.setText(mDataset.get(position).getIdUsername());
        //holder.txtViewId.setActivated(true);
        //holder.txtViewTitle.setActivated(true);
        //View.OnClickListener myListener = new MyOnClickListenerItem(context,position,mDataset);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView txtViewId;
        public TextView txtViewTitle;
        public TextView txtViewUsername;
        public MyViewHolder(View v) {
            super(v);
            txtViewId = v.findViewById(R.id.itemValueId);
            txtViewTitle = v.findViewById(R.id.itemValueTitle);
            txtViewUsername = v.findViewById(R.id.itemValueUsername);
        }
    }


}
