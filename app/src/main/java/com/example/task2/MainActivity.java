package com.example.task2;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.example.task2.dao.ListaDao;
import com.example.task2.dao.UserDao;
import com.example.task2.dao.impl.ListaDaoImpl;
import com.example.task2.dao.impl.UserDaoImpl;
import com.example.task2.entity.Lista;
import com.example.task2.entity.User;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private static final String USER1 = "st";
    private static final String USER2 = "ab";
    private static final String TAG = "MainActivity";
    public static final String EXTRA_USER = "COM.ENTITY.USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        UserDao userDao = new UserDaoImpl();
        User user = userDao.findById(USER1,getApplicationContext());
        Log.i(TAG,"validando usuario 1");
        if(user!=null)
        {
            Log.i(TAG,"usuario 1 ya existe");
        }
        else
        {
            Log.i(TAG,"insertando usuario 1");
            user = new User();
            user.setUsername(USER1);
            user.setPassword("1");
            user.setFirstName("Abraham");
            user.setLastName("Lugo");
            user.setAvatar("user_3");
            userDao.insert(user,getApplicationContext());
            Log.i(TAG,"agregando peliculas");
            ListaDao listaDao = new ListaDaoImpl();
            Lista lista1 = new Lista();
            lista1.setTitle("El Rey Leon");
            lista1.setIdUsername(user.getUsername());
            listaDao.insert(lista1,getApplicationContext());

            Lista lista2 = new Lista();
            lista2.setTitle("V de Vendeta");
            lista2.setIdUsername(user.getUsername());
            listaDao.insert(lista2,getApplicationContext());

            Lista lista3 = new Lista();
            lista3.setTitle("Once Upon Time in Mexico");
            lista3.setIdUsername(user.getUsername());
            listaDao.insert(lista3,getApplicationContext());

            Lista lista4 = new Lista();
            lista4.setTitle("Matrix");
            lista4.setIdUsername(user.getUsername());
            listaDao.insert(lista4,getApplicationContext());

            Lista lista5 = new Lista();
            lista5.setTitle("Harry Potter");
            lista5.setIdUsername(user.getUsername());
            listaDao.insert(lista5,getApplicationContext());


        }

        user = userDao.findById(USER2,getApplicationContext());
        Log.i(TAG,"validando usuario 2");
        if(user!=null)
        {
            Log.i(TAG,"usuario 2 ya existe");
        }
        else
        {
            Log.i(TAG,"insertando usuario 2");
            user = new User();
            user.setUsername(USER2);
            user.setPassword("2");
            user.setFirstName("Abraham2");
            user.setLastName("Martinez");
            user.setAvatar("user_4");
            userDao.insert(user,getApplicationContext());
        }
    }

    public void validateLogin(View view)
    {
        Log.i(TAG,"validando autenticacion...");
        EditText usernameTxt = findViewById(R.id.editTextTextPersonName2);
        EditText passwordTxt = findViewById(R.id.editTextTextPassword2);
        String usernameStr = usernameTxt.getText().toString();
        String passwordStr = passwordTxt.getText().toString();
        Log.i(TAG,"username='" + usernameStr + "', pass='" + passwordStr + "'");

        UserDao userDao = new UserDaoImpl();
        User user = userDao.findById(usernameStr,getApplicationContext());
        if(user!=null && passwordStr.equals(user.getPassword()))
        {
            Log.i(TAG,"acceso correcto");
            String lastAccess = user.getLastAccess();
            Date access = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss_dd.MM.yyyy");
            user.setLastAccess(simpleDateFormat.format(access));
            userDao.update(user,getApplicationContext());
            Intent intent = new Intent(this, ShowList.class);
            intent.putExtra(MainActivity.EXTRA_USER,user);
            intent.putExtra("ULTIMO_ACCESO",lastAccess);
            startActivity(intent);
        }
        else
        {
            Log.i(TAG,"acceso denegado");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}