package com.example.task2.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.util.Log;
import com.example.task2.dao.UserDao;
import com.example.task2.entity.DBContract;
import com.example.task2.entity.DBHelper;
import com.example.task2.entity.User;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private static final String TAG = "UserDaoImpl";


    @Override
    public List<User> getAll(Context context) {
        List<User> answer = new ArrayList<>();
        DBHelper dbHelper = new DBHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                DBContract.User.COLUMN_NAME_USERNAME,
                DBContract.User.COLUMN_NAME_PASSWORD,
                DBContract.User.COLUMN_NAME_FIRST_NAME,
                DBContract.User.COLUMN_NAME_LAST_NAME,
                DBContract.User.COLUMN_NAME_LAST_ACCESS,
                DBContract.User.COLUMN_NAME_AVATAR
    };
    Cursor cursor = db.query(
            DBContract.User.TABLE_NAME,   // The table to query
            projection,             // The array of columns to return (pass null to get all)
            null,              // The columns for the WHERE clause
            null,          // The values for the WHERE clause
            null,          // don't group the rows
            null,           // don't filter by row groups
            null               // The sort order
    );
    while(cursor.moveToNext())
    {
        User usrTmp = new User();
        usrTmp.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.User._ID)));
        usrTmp.setUsername(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_USERNAME)));
        usrTmp.setPassword(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_PASSWORD)));
        usrTmp.setFirstName(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_FIRST_NAME)));
        usrTmp.setLastName(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_LAST_NAME)));
        usrTmp.setLastAccess(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_LAST_ACCESS)));
        usrTmp.setAvatar(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_AVATAR)));
        answer.add(usrTmp);
    }
    cursor.close();
        return answer;
    }

    @Override
    public List<User> loadAllByIds(String[] userIds,Context context) {
        return null;
    }

    @Override
    public User findByName(String first, String last,Context context) {
        return null;
    }

    @Override
    public User findById(String usrname,Context context) {
        User answer = null;
        DBHelper dbHelper = new DBHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                DBContract.User.COLUMN_NAME_USERNAME,
                DBContract.User.COLUMN_NAME_PASSWORD,
                DBContract.User.COLUMN_NAME_FIRST_NAME,
                DBContract.User.COLUMN_NAME_LAST_NAME,
                DBContract.User.COLUMN_NAME_LAST_ACCESS,
                DBContract.User.COLUMN_NAME_AVATAR
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = DBContract.User.COLUMN_NAME_USERNAME + " = ?";
        String[] selectionArgs = { usrname };

        Cursor cursor = db.query(
                DBContract.User.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,          // don't group the rows
                null,           // don't filter by row groups
                null               // The sort order
        );

        while(cursor.moveToNext())
        {
            answer = new User();
            answer.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.User._ID)));
            answer.setUsername(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_USERNAME)));
            answer.setPassword(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_PASSWORD)));
            answer.setFirstName(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_FIRST_NAME)));
            answer.setLastName(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_LAST_NAME)));
            answer.setLastAccess(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_LAST_ACCESS)));
            answer.setAvatar(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.User.COLUMN_NAME_AVATAR)));
        }
        cursor.close();
        return answer;
    }

    @Override
    public void insertAll(Context context,User... users) {

    }

    @Override
    public void update(User user,Context context) {

        DBHelper dbHelper = new DBHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DBContract.User.COLUMN_NAME_USERNAME, user.getUsername());
        values.put(DBContract.User.COLUMN_NAME_PASSWORD, user.getPassword());
        values.put(DBContract.User.COLUMN_NAME_FIRST_NAME, user.getFirstName());
        values.put(DBContract.User.COLUMN_NAME_LAST_NAME, user.getLastName());
        values.put(DBContract.User.COLUMN_NAME_AVATAR, user.getAvatar());
        values.put(DBContract.User.COLUMN_NAME_LAST_ACCESS, user.getLastAccess());

        // Which row to update, based on the title
        String idStr = String.valueOf(user.getId());
        String selectionUpdate = DBContract.User._ID + " = ?";
        String[] selectionArgsUpdate = { idStr };

        int count = db.update(
                DBContract.User.TABLE_NAME,
                values,
                selectionUpdate,
                selectionArgsUpdate);
        Log.i(TAG,"update :: " + count);
    }

    @Override
    public void insert(User user,Context context) {
        DBHelper dbHelper = new DBHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBContract.User.COLUMN_NAME_USERNAME, user.getUsername());
        values.put(DBContract.User.COLUMN_NAME_PASSWORD, user.getPassword());
        values.put(DBContract.User.COLUMN_NAME_FIRST_NAME, user.getFirstName());
        values.put(DBContract.User.COLUMN_NAME_LAST_NAME, user.getLastName());
        values.put(DBContract.User.COLUMN_NAME_AVATAR, user.getAvatar());
        long newRowId = db.insert(DBContract.User.TABLE_NAME, null, values);
        Log.i(TAG,"[insert]newRowId= " + newRowId);
    }

    @Override
    public void delete(User user,Context context) {

    }
}
