package com.example.task2.dao;
import android.content.Context;
import com.example.task2.entity.Lista;
import java.util.List;

public interface ListaDao {
    List<Lista> getAll(Context context);
    public List<Lista> getListsByUser(String usrname,Context context);
    public void insert(Lista lista,Context context);

    void delete(Lista lista, Context context);

    Lista getListaById(String id,Context context);
}
