package com.example.task2.dao;

import android.content.Context;

import com.example.task2.entity.User;

import java.util.List;

public interface UserDao {
    List<User> getAll(Context context);
    List<User> loadAllByIds(String[] userIds,Context context);
    User findByName(String first, String last,Context context);
    User findById(String usrname,Context context);
    void insertAll(Context context,User... users);
    void update(User user,Context context);
    void insert(User user,Context context);
    void delete(User user,Context context);
}
