package com.example.task2.dao.impl;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import com.example.task2.dao.ListaDao;
import com.example.task2.entity.DBContract;
import com.example.task2.entity.DBHelper;
import com.example.task2.entity.Lista;

import java.util.ArrayList;
import java.util.List;

public class ListaDaoImpl implements ListaDao {

    @Override
    public List<Lista> getAll(Context context) {
        return null;
    }

    @Override
    public List<Lista> getListsByUser(String usrname,Context context) {
        List<Lista> myDataSet = new ArrayList<Lista>();
        DBHelper dbHelper = new DBHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        String[] projection = {
                BaseColumns._ID,
                DBContract.Lista.COLUMN_NAME_TITLE,
                DBContract.Lista.COLUMN_NAME_USERNAME
        };
        String selection = DBContract.Lista.COLUMN_NAME_USERNAME + " = ?";
        String[] selectionArgs = { usrname };

        Cursor cursor = db.query(
                DBContract.Lista.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,          // don't group the rows
                null,           // don't filter by row groups
                null               // The sort order
        );
        while(cursor.moveToNext())
        {
            Lista tmp = new Lista();
            tmp.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.Lista._ID)));
            tmp.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.Lista.COLUMN_NAME_TITLE)));
            tmp.setIdUsername(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.Lista.COLUMN_NAME_USERNAME)));
            myDataSet.add(tmp);
        }
        cursor.close();
        return myDataSet;
    }

    @Override
    public void insert(Lista lista,Context context) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(DBContract.Lista.COLUMN_NAME_TITLE, lista.getTitle());
        values.put(DBContract.Lista.COLUMN_NAME_USERNAME, lista.getIdUsername());
        long newRowId = db.insert(DBContract.Lista.TABLE_NAME, null, values);
    }

    @Override
    public void delete(Lista lista, Context context) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String whereClause = DBContract.Lista._ID + "=?";
        String idStr = String.valueOf(lista.getId());
        String[] whereClauseArgs = { idStr };
        db.delete(DBContract.Lista.TABLE_NAME,whereClause,whereClauseArgs);

    }

    @Override
    public Lista getListaById(String id,Context context) {
        Lista answer = null;
        DBHelper dbHelper = new DBHelper(context);
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                DBContract.Lista.COLUMN_NAME_TITLE,
                DBContract.Lista.COLUMN_NAME_USERNAME
        };
        String selection = DBContract.Lista._ID + " = ?";
        String[] selectionArgs = { id };

        Cursor cursor = db.query(
                DBContract.Lista.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                selection,              // The columns for the WHERE clause
                selectionArgs,          // The values for the WHERE clause
                null,          // don't group the rows
                null,           // don't filter by row groups
                null               // The sort order
        );
        while(cursor.moveToNext())
        {
            answer = new Lista();
            answer.setId(cursor.getInt(cursor.getColumnIndexOrThrow(DBContract.Lista._ID)));
            answer.setTitle(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.Lista.COLUMN_NAME_TITLE)));
            answer.setIdUsername(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.Lista.COLUMN_NAME_USERNAME)));
        }
        cursor.close();
        return answer;
    }
}
