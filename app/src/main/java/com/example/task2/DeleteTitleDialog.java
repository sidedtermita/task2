package com.example.task2;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.RecyclerView;
import com.example.task2.dao.ListaDao;
import com.example.task2.dao.impl.ListaDaoImpl;
import com.example.task2.entity.Lista;
import java.util.List;

public class DeleteTitleDialog extends DialogFragment {

    private static final String TAG = "deleteTitleDialog";
    private RecyclerView recyclerView;
    private Lista lista;

    public DeleteTitleDialog(Lista lista,RecyclerView recyclerView)
    {
        this.lista = lista;
        this.recyclerView = recyclerView;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = requireActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.delete_view_dialog, null);
        TextView textView = view.findViewById(R.id.txtMsgDeleteConfirmation);
        textView.setText("Seguro que desea elimianr " + lista.getTitle() + "?");
        builder.setTitle("Eliminar...");
        builder.setView(view)
                .setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i(TAG,"conectando a db...");
                        ListaDao listaDao = new ListaDaoImpl();
                        String username = lista.getIdUsername();
                        listaDao.delete(lista,getContext());
                        List<Lista> myDataSet = listaDao.getListsByUser(username,getContext());
                        MyAdapter myAdapter = new MyAdapter(myDataSet, getContext());
                        myAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(myAdapter);//recyclerView
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        return builder.create();
    }
}
